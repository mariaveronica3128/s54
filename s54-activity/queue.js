let collection = [];

// Write the queue functions below.
// Output all the elements of the queue

function print () {
    return collection
}

// add element to the rear of queue

function enqueue(item) {
    collection.push(item)
    return collection
}

// remove element at front of queue

function dequeue (item) {
    collection.shift(item)
    return collection
}

// show elements at the front

function front () {

    const first = collection[0]
    return first
}

// show total number of elements
    function size (){
        return collection.length
    }

// Outputs a Boolean value describing whether queue is empty or not
function isEmpty() {

    let status = null

        if (collection.length == 0) {
            status = true
        } else {
            status = false
        }

    return status
    }




module.exports = {

    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty

};